from rest_framework.renderers import JSONRenderer


class FrostyApiRenderer(JSONRenderer):
    media_type = 'application/vnd.frosty+json'
