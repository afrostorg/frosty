From python:3.8.2-slim-buster AS build-image

RUN apt-get update --fix-missing && apt-get -y --no-install-recommends install git \
python3-dev \
libpq-dev \
gcc \
build-essential \
zlib1g-dev \
libjpeg-dev \
liblcms2-dev \
libtiff-dev \
libfreetype6-dev \
libwebp-dev \
tcl8.6-dev \
tk8.6-dev \
python-tk \
libmagic-dev

ENV PATH="/root/.local/bin:${PATH}"


RUN pip install --user --upgrade pip

ADD ./requirements /opt/requirements
RUN pip install --user -r /opt/requirements/production.txt
RUN pip install --user django-debug-toolbar==2.2 uwsgi

RUN cd /root/.local/lib/python3.8/site-packages/ && rm -rf  fabfile* fabric* ipdb* ipython* virtualenv* IPython* 56fa58acf89604978a41__mypyc.cpython-38-x86_64-linux-gnu.so

# build an archive of all linked libraries
RUN ldd $(find /root/.local/lib/python3.8/site-packages -name '*.so*') |  grep -vE "not a dynamic executable|not found|^$|:$"| cut -d" " -f3 | sort | uniq | xargs tar --dereference -cf /libs.tar


# build the deploy image
FROM python:3.8.2-slim-buster AS deploy-image

COPY --from=build-image /root/.local /root/.local
COPY --from=build-image /usr/lib/x86_64-linux-gnu/libmagic*  /usr/lib/x86_64-linux-gnu/
COPY --from=build-image /usr/bin/pg_config /usr/bin/pg_config
COPY --from=build-image /libs.tar /

RUN tar -xvf libs.tar && rm /libs.tar

# Make sure scripts in .local are usable:
ENV PATH=/root/.local/bin:$PATH

ADD . /opt/frosty
WORKDIR /opt/frosty

ENTRYPOINT ["uwsgi", "--http", "0.0.0.0:80", "--module", "wsgi:application"]
